<?php
    require_once('koneksi.php');
    date_default_timezone_set("Asia/Jakarta");

    $id = $_GET['id'];
    $user_id = $_GET['gambar'];
    $target_dir     = "Gambar/";
    $target_file    =  $user_id.".jpg";

    if(empty($id)) {
        $response = new \stdClass();
        $response->status = 100;
        $response->message = "Data harus komplit.";
        die(json_encode($response));
    } else {
        $query = mysqli_query($koneksi,"DELETE FROM tabel_user WHERE id ='$id'");
        if ($query){
            unlink( $target_dir . $target_file);
            $response = new \stdClass();
            $response->status = 200;
            $response->message = "Hapus Berhasil.";
            die(json_encode($response));
        } else {
            $response = new \stdClass();
            $response->status = 100;
            $response->message = "Gagal.";
            die(json_encode($response));
        }
    }

?>