<?php
    require_once('koneksi.php');
    date_default_timezone_set("Asia/Jakarta");

        //Variable
        $user_id   = ($_POST["user_id"]);
        $password  = ($_POST["password"]);
        $last_login = date("Y-m-d H:i:sa");

    if(empty($user_id) || empty($password) ) {
        $response = new \stdClass();
        $response->status = 100;
        $response->message = "Username atau password tidak boleh kosong.";
        die(json_encode($response));
    } else {
        $query = mysqli_query($koneksi,"SELECT * FROM tabel_user WHERE `user_id` = '$user_id' AND `password` = '$password'");
        $row = mysqli_fetch_assoc($query);
        if (!empty($row)){
            $response = new \stdClass();
            $response->statusLogin = true;
            $response->status = 200;
            $response->id = $row['id'];
            $response->user_id = $row['user_id'];
            $response->message = "Login Berhasil.";
            $response->nama = $row['nama'];
            $response->alamat = $row['alamat'];
            $response->pekerjaan = $row['pekerjaan'];
            $response->foto = $row['foto'];
            $response->login = $last_login;
            die(json_encode($response));
        } else {
            $response = new \stdClass();
            $response->statusLogin = false;
            $response->status = 100;
            $response->message = "Username atau password salah.";
            die(json_encode($response));
        }
    }
    
?>