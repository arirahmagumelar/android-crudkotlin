package dev.arg.tribintang.crudkotlin.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ModelStatusMessage : Serializable {
    @SerializedName("status")
    var status: Int = 0

    @SerializedName("message")
    var message: String? = null
}