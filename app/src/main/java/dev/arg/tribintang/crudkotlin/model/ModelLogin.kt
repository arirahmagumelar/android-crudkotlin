package dev.arg.tribintang.crudkotlin.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ModelLogin : Serializable {
    @SerializedName("id")
    var id: String? = null

    @SerializedName("user_id")
    var user_id: String? = null

    @SerializedName("statusLogin")
    var statusLogin: Boolean = true

    @SerializedName("status")
    var status: Int = 0

    @SerializedName("message")
    var message: String? = null

    @SerializedName("nama")
    var nama: String? = null

    @SerializedName("alamat")
    var alamat: String? = null

    @SerializedName("pekerjaan")
    var pekerjaan: String? = null

    @SerializedName("login")
    var login: String? = null

    @SerializedName("foto")
    var foto: String? = null
}