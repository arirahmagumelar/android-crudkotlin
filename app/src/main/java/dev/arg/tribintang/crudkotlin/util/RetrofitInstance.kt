package dev.arg.tribintang.crudkotlin.util

import android.content.Context
import android.net.ConnectivityManager
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*
import java.util.concurrent.TimeUnit

class RetrofitInstance {

    fun getRetrofitInstance() : InterfaceRetrofit{
        val retrofit = Retrofit.Builder()
                .baseUrl(HardCode.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(untukTimeOutRetrofit())
                .build()
        return retrofit.create(InterfaceRetrofit::class.java)
    }

    private fun untukTimeOutRetrofit(): OkHttpClient {
        return OkHttpClient.Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .readTimeout(40, TimeUnit.SECONDS)
                .build()
    }

    fun checkKoneksi(context: Context): Boolean {
        return (Objects.requireNonNull(context.getSystemService(Context.CONNECTIVITY_SERVICE)) as ConnectivityManager).activeNetworkInfo != null
    }

    fun createPartFromString(descriptionString: String): RequestBody {
        return RequestBody.create(
            MultipartBody.FORM, descriptionString)
    }
}