package dev.arg.tribintang.crudkotlin.ui.register

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.ImageDecoder
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import cn.pedant.SweetAlert.SweetAlertDialog
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import dev.arg.tribintang.crudkotlin.R
import dev.arg.tribintang.crudkotlin.model.ModelStatusMessage
import dev.arg.tribintang.crudkotlin.util.FileUtils
import dev.arg.tribintang.crudkotlin.util.Gambar
import dev.arg.tribintang.crudkotlin.util.Gambar.createImageData
import dev.arg.tribintang.crudkotlin.util.HardCode.Companion.PICK_IMAGE_REQUEST
import dev.arg.tribintang.crudkotlin.util.RetrofitInstance
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import pub.devrel.easypermissions.EasyPermissions
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.File
import java.util.*

class ActivityRegister : AppCompatActivity() {
    //Design
    lateinit var etUsername: EditText
    lateinit var etPassword: EditText
    lateinit var etNama: EditText
    lateinit var etAlamat: EditText
    lateinit var etPekerjaan: EditText
    lateinit var btRegister: Button
    lateinit var btPilihGambar: Button
    lateinit var imPreview: ImageView
    lateinit var tvAlready: TextView

    //Value
    lateinit var rbs: String
    lateinit var model: ModelStatusMessage
    lateinit var multipartBody: MultipartBody.Part
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        etUsername = findViewById(R.id.etUserId)
        etPassword = findViewById(R.id.etPassword)
        etNama     = findViewById(R.id.etNama)
        etAlamat   = findViewById(R.id.etAlamat)
        etPekerjaan= findViewById(R.id.etPekerjaan)
        btRegister   = findViewById(R.id.btRegister)
        btPilihGambar = findViewById(R.id.btPilihGambar)
        imPreview  = findViewById(R.id.imPreview)
        tvAlready  = findViewById(R.id.tvAlready)

        btRegister.setOnClickListener{ register() }

        btPilihGambar.setOnClickListener(onPilihGambar)
        tvAlready.setOnClickListener {
            onBackPressed()
        }

    }

    private val onPilihGambar = View.OnClickListener {
        if (EasyPermissions.hasPermissions(this,android.Manifest.permission.READ_EXTERNAL_STORAGE)){
            showFileChooser()
        } else {
            EasyPermissions.requestPermissions(this,"Aplikasi ini membutuhkan perizinan akses galeri..\nSilahkan klik OK",991,android.Manifest.permission.READ_EXTERNAL_STORAGE)
        }
    }

    private fun showFileChooser() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST)
    }

    private fun setToImageView(bmp: Bitmap) { //compress image
        val bytes = ByteArrayOutputStream()
        bmp.compress(
            Bitmap.CompressFormat.JPEG, 100 // range 1 - 100
            , bytes)
         val decoded = BitmapFactory.decodeStream(ByteArrayInputStream(bytes.toByteArray()))
        //menampilkan gambar yang dipilih dari camera/gallery ke ImageView
//        Glide.with(this)
//            .asBitmap()
//            .load(decoded)
//            .apply(RequestOptions().centerCrop())
//            .into(imPreview)
        imPreview.setImageBitmap(decoded)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK && data != null && data.data != null) {
            val filePath = data.data!!
            val file = FileUtils.getFile(this,filePath)!!
            multipartBody = multiPart(filePath,file)
            try {
                filePath.let {
                    if(Build.VERSION.SDK_INT < 28) {
                        val bitmap = MediaStore.Images.Media.getBitmap(
                            contentResolver,
                            filePath
                        )
                        // 512 adalah resolusi tertinggi setelah image di resize, bisa di ganti.
                        setToImageView(Gambar.getResizedBitmap(bitmap, 2024)!!)
                    } else {
                        val source = ImageDecoder.createSource(contentResolver, filePath)
                        val bitmap = ImageDecoder.decodeBitmap(source)
                        setToImageView(Gambar.getResizedBitmap(bitmap, 2024)!!)

                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun multiPart(uri: Uri, file: File) : MultipartBody.Part{
        val requestFile = RequestBody.create(MediaType.parse("image/*"),createImageData(this,uri)!!)
        return MultipartBody.Part.createFormData("bukti_pembayaran",file.name.toLowerCase(Locale.ROOT), requestFile)
    }

    //Register CRUD - Insert
    private fun register(){
        val pDialog =  SweetAlertDialog(this@ActivityRegister, SweetAlertDialog.PROGRESS_TYPE)
        pDialog.progressHelper.barColor = ContextCompat.getColor(this@ActivityRegister, R.color.colorPrimary)
        pDialog.titleText = "Loading"
        pDialog.contentText = "Sedang mengupload data.."
        pDialog.setCancelable(false)
        pDialog.show()

        if (RetrofitInstance().checkKoneksi(this)) {
            val a = RetrofitInstance().createPartFromString(etUsername.text.toString())
            val b = RetrofitInstance().createPartFromString(etPassword.text.toString())
            val c = RetrofitInstance().createPartFromString(etNama.text.toString())
            val d = RetrofitInstance().createPartFromString(etAlamat.text.toString())
            val e = RetrofitInstance().createPartFromString(etPekerjaan.text.toString())
            val service = RetrofitInstance().getRetrofitInstance()
            val call = service.registerAkun(a,b,c,d,e,multipartBody)
            call.enqueue(object : Callback<ResponseBody> {
                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    if (response.isSuccessful) {
                        try {
                            rbs = Objects.requireNonNull<ResponseBody>(response.body()).string()
                            Log.e("respon",rbs)
                            model = Gson().fromJson(rbs, object : TypeToken<ModelStatusMessage>() {}.type)

                            if (model.status == 200) {
                                Toast.makeText(this@ActivityRegister, model.message, Toast.LENGTH_SHORT).show()
                                onBackPressed()
                            } else {
                                Toast.makeText(this@ActivityRegister, model.message, Toast.LENGTH_SHORT).show()
                            }

                            pDialog.dismiss()

                        } catch (e: Exception) {
                            pDialog.dismiss()
                            e.printStackTrace()
                        }

                    } else {
                        pDialog.dismiss()
                        Toast.makeText(this@ActivityRegister, response.message(), Toast.LENGTH_SHORT).show()
                    }
                }

                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    pDialog.dismiss()
                    Toast.makeText(this@ActivityRegister, t.message, Toast.LENGTH_SHORT).show()
                }
            })

        } else {
            pDialog.dismiss()
            Toast.makeText(this@ActivityRegister, "Periksa Jaringan anda.", Toast.LENGTH_SHORT).show()
        }
    }
}