package dev.arg.tribintang.crudkotlin.ui.login

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import cn.pedant.SweetAlert.SweetAlertDialog
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import dev.arg.tribintang.crudkotlin.R
import dev.arg.tribintang.crudkotlin.model.ModelLogin
import dev.arg.tribintang.crudkotlin.ui.HomeActivity
import dev.arg.tribintang.crudkotlin.ui.register.ActivityRegister
import dev.arg.tribintang.crudkotlin.util.RetrofitInstance
import dev.arg.tribintang.crudkotlin.util.SharedPrefManager
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class ActivityLogin : AppCompatActivity() {
    //Design
    lateinit var tvRegister: TextView
    lateinit var etUsername: EditText
    lateinit var etPassword: EditText

    lateinit var btLogin: Button

    //Value
    lateinit var rbs: String
    lateinit var model: ModelLogin


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        etUsername = findViewById(R.id.etUserId)
        etPassword = findViewById(R.id.etPassword)

        //Mengecek session jika sudah login maka langsung ke Home
        if (SharedPrefManager(this).spSudahLogin!!) {
            bypassLogin()
        }

        tvRegister = findViewById(R.id.tvRegister)
        tvRegister.setOnClickListener {
            tidakPunyaAkun()
        }

        btLogin    = findViewById(R.id.btLogin)
        btLogin.setOnClickListener {
            login()
        }

    }

    private fun tidakPunyaAkun(){
        val intent = Intent(this, ActivityRegister::class.java)
        startActivity(intent)
    }

    private fun bypassLogin(){
        val intent = Intent(this, HomeActivity::class.java)
            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)

        finish()
    }


    //Login CRUD - Select
    private fun login(){
        val pDialog =  SweetAlertDialog(this@ActivityLogin, SweetAlertDialog.PROGRESS_TYPE)
        pDialog.progressHelper.barColor = ContextCompat.getColor(this@ActivityLogin, R.color.colorPrimary)
        pDialog.titleText = "Loading"
        pDialog.contentText = "Sedang check data user.."
        pDialog.setCancelable(false)
        pDialog.show()

        if (RetrofitInstance().checkKoneksi(this)) {
            val a = etUsername.text.toString()
            val b = etPassword.text.toString()
            val service = RetrofitInstance().getRetrofitInstance()
            val call = service.loginAkun(a,b)
            call.enqueue(object : Callback<ResponseBody> {
                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    if (response.isSuccessful) {
                        try {
                            rbs = Objects.requireNonNull<ResponseBody>(response.body()).string()
                            Log.e("response", rbs)
                            model = Gson().fromJson(rbs, object : TypeToken<ModelLogin>() {}.type)

                            //Jika status 200 atau berhasil, maka menyimpan sesi ke SharedPrefManager
                            if (model.status == 200) {
                                Toast.makeText(this@ActivityLogin, model.message, Toast.LENGTH_SHORT).show()
                                menyimpanSesi(model)
                            } else {
                                Toast.makeText(this@ActivityLogin, model.message, Toast.LENGTH_SHORT).show()
                            }

                            pDialog.dismiss()

                        } catch (e: Exception) {
                            pDialog.dismiss()
                            Toast.makeText(this@ActivityLogin, e.message, Toast.LENGTH_SHORT).show()
                            e.printStackTrace()
                        }

                    } else {
                        pDialog.dismiss()
                        Toast.makeText(this@ActivityLogin, response.message(), Toast.LENGTH_SHORT).show()
                    }
                }

                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    pDialog.dismiss()
                    Toast.makeText(this@ActivityLogin, t.message, Toast.LENGTH_SHORT).show()
                }
            })

        } else {
            pDialog.dismiss()
            Toast.makeText(this@ActivityLogin, "Periksa Jaringan anda.", Toast.LENGTH_SHORT).show()
        }
    }

    fun menyimpanSesi(model: ModelLogin){
        SharedPrefManager(this@ActivityLogin).saveSPBoolean(SharedPrefManager.SP_SUDAH_LOGIN, model.statusLogin)
        SharedPrefManager(this@ActivityLogin).saveSPString(SharedPrefManager.SP_NAMA, model.nama!!)
        SharedPrefManager(this@ActivityLogin).saveSPString(SharedPrefManager.SP_ALAMAT, model.alamat!!)
        SharedPrefManager(this@ActivityLogin).saveSPString(SharedPrefManager.SP_PEKERJAAN, model.pekerjaan!!)
        SharedPrefManager(this@ActivityLogin).saveSPString(SharedPrefManager.SP_ID, model.id!!)
        SharedPrefManager(this@ActivityLogin).saveSPString(SharedPrefManager.SP_USER, model.user_id!!)
        SharedPrefManager(this@ActivityLogin).saveSPString(SharedPrefManager.SP_FOTO, model.foto!!)
        bypassLogin()
    }

    override fun onBackPressed() {
        val intent = Intent(Intent.ACTION_MAIN)
        intent.addCategory(Intent.CATEGORY_HOME)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
    }
}