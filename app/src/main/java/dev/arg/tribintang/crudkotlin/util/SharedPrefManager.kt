package dev.arg.tribintang.crudkotlin.util

import android.content.Context
import android.content.SharedPreferences

class SharedPrefManager(context: Context) {
    private var sp: SharedPreferences
    private var spEditor: SharedPreferences.Editor

    val spId: String? get() = sp.getString(SP_ID, "")
    val spUser: String? get() = sp.getString(SP_USER, "")
    val spFoto: String? get() = sp.getString(SP_FOTO, "")
    val spNama: String? get() = sp.getString(SP_NAMA, "")
    val spAlamat: String? get() = sp.getString(SP_ALAMAT, "")
    val spPekerjaan: String? get() = sp.getString(SP_PEKERJAAN, "")
    val spLogin: String? get() = sp.getString(SP_LOGIN,"")

    val spSudahLogin: Boolean?
        get() = sp.getBoolean(SP_SUDAH_LOGIN, false)

    val spBelumLogin: Boolean?
        get() = sp.getBoolean(SP_BELUM_LOGIN, false)

    init {
        sp = context.getSharedPreferences(SP_OWNER, Context.MODE_PRIVATE)
        spEditor = sp.run { edit() }
    }

    fun saveSPString(keySP: String, value: String) {
        spEditor.putString(keySP, value)
        spEditor.commit()
    }

    fun saveSPInt(keySP: String, value: Int) {
        spEditor.putInt(keySP, value)
        spEditor.commit()
    }

    fun saveSPBoolean(keySP: String, value: Boolean) {
        spEditor.putBoolean(keySP, value)
        spEditor.commit()
    }

    companion object {
        val SP_OWNER = "spDevArg"
        val SP_ID = "spId"
        val SP_USER = "spUser"
        val SP_NAMA = "spNama"
        val SP_ALAMAT = "spAlamat"
        val SP_PEKERJAAN = "spPekerjaan"
        val SP_LOGIN = "spLogin"
        val SP_FOTO = "spFoto"

        val SP_SUDAH_LOGIN = "spSudahLogin"
        val SP_BELUM_LOGIN = "spBelumLogin"
    }

    fun userID(): String {
        return sp.getString(SP_ID, "").toString()
    }
    fun user(): String {
        return sp.getString(SP_USER, "").toString()
    }
    fun getFoto(): String {
        return sp.getString(SP_FOTO, "").toString()
    }

}