package dev.arg.tribintang.crudkotlin.ui.akun

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.ImageDecoder
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import cn.pedant.SweetAlert.SweetAlertDialog
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import dev.arg.tribintang.crudkotlin.R
import dev.arg.tribintang.crudkotlin.model.ModelStatusMessage
import dev.arg.tribintang.crudkotlin.ui.login.ActivityLogin
import dev.arg.tribintang.crudkotlin.util.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import pub.devrel.easypermissions.EasyPermissions
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.File
import java.util.*

class AkunActivity : AppCompatActivity() {
    //Design
    lateinit var etUserId: EditText
    lateinit var etPassword: EditText
    lateinit var etNama: EditText
    lateinit var etAlamat: EditText
    lateinit var etPekerjaan: EditText
    lateinit var btLogout: Button
    lateinit var btEdit: Button
    lateinit var btDelete: Button
    lateinit var btPilihGambar: Button

    lateinit var imPreview: ImageView


    //Value
    var multipartBody: MultipartBody.Part? = null
    lateinit var rbs: String
    lateinit var model: ModelStatusMessage
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account)

        etUserId   = findViewById(R.id.etUserId)
        etPassword = findViewById(R.id.etPassword)
        etNama     = findViewById(R.id.etNama)
        etAlamat   = findViewById(R.id.etAlamat)
        etPekerjaan= findViewById(R.id.etPekerjaan)
        imPreview  = findViewById(R.id.imPreview)
        btPilihGambar = findViewById(R.id.btPilihGambar)

        btLogout = findViewById(R.id.btLogout)
        btLogout.setOnClickListener {
            logout()
        }

        btEdit = findViewById(R.id.btEdit)
        btEdit.setOnClickListener {
            if (multipartBody != null) {
                edit()
            } else {
                Toast.makeText(this@AkunActivity, "Gambar harus diisi..", Toast.LENGTH_SHORT).show()
            }
        }

        btDelete = findViewById(R.id.btDelete)
        btDelete.setOnClickListener {
            delete()
        }

        btPilihGambar.setOnClickListener(onPilihGambar)
        etUserId.isEnabled = false
        etUserId.setText(String.format("%s", SharedPrefManager(this).user()))
    }

    private val onPilihGambar = View.OnClickListener {
        if (EasyPermissions.hasPermissions(this@AkunActivity,android.Manifest.permission.READ_EXTERNAL_STORAGE)){
            showFileChooser()
        } else {
            EasyPermissions.requestPermissions(this@AkunActivity,"Aplikasi ini membutuhkan perizinan akses galeri..\nSilahkan klik OK",991,android.Manifest.permission.READ_EXTERNAL_STORAGE)
        }
    }

    private fun showFileChooser() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(
            Intent.createChooser(intent, "Select Picture"),
            HardCode.PICK_IMAGE_REQUEST
        )
    }
    private fun setToImageView(bmp: Bitmap) { //compress image
        val bytes = ByteArrayOutputStream()
        bmp.compress(
            Bitmap.CompressFormat.JPEG, 100 // range 1 - 100
            , bytes)
        val decoded = BitmapFactory.decodeStream(ByteArrayInputStream(bytes.toByteArray()))
        //menampilkan gambar yang dipilih dari camera/gallery ke ImageView
//        Glide.with(this)
//            .asBitmap()
//            .load(decoded)
//            .apply(RequestOptions().centerCrop())
//            .into(imPreview)
        imPreview.setImageBitmap(decoded)
    }

    //Hapus Session.
    fun logout(){
        SharedPrefManager(this@AkunActivity).saveSPBoolean(SharedPrefManager.SP_SUDAH_LOGIN, false)
        val intent = Intent(this@AkunActivity, ActivityLogin::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
        finish()
    }

    //Edit CRUD - Edit (Update)
    private fun edit(){
        val pDialog =  SweetAlertDialog(this@AkunActivity, SweetAlertDialog.PROGRESS_TYPE)
        pDialog.progressHelper.barColor = ContextCompat.getColor(this@AkunActivity, R.color.colorPrimary)
        pDialog.titleText = "Loading"
        pDialog.contentText = "Sedang mengupload data.."
        pDialog.setCancelable(false)
        pDialog.show()

        if (RetrofitInstance().checkKoneksi(this)) {
            val id =  RetrofitInstance().createPartFromString(SharedPrefManager(this).userID())
            val a =  RetrofitInstance().createPartFromString(etPassword.text.toString())
            val b =  RetrofitInstance().createPartFromString(etNama.text.toString())
            val c =  RetrofitInstance().createPartFromString(etAlamat.text.toString())
            val d =  RetrofitInstance().createPartFromString(etPekerjaan.text.toString())
            val e = RetrofitInstance().createPartFromString(SharedPrefManager(this).user())
            val service = RetrofitInstance().getRetrofitInstance()
            val call = service.editAkun(id,a,b,c,d,e,multipartBody!!)
            call.enqueue(object : Callback<ResponseBody> {
                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    if (response.isSuccessful) {
                        try {
                            rbs = Objects.requireNonNull<ResponseBody>(response.body()).string()
                            Log.e("rbs", rbs)
                            model = Gson().fromJson(rbs, object : TypeToken<ModelStatusMessage>() {}.type)

                            if (model.status == 200) {
                                Toast.makeText(this@AkunActivity, model.message, Toast.LENGTH_SHORT).show()
                                onBackPressed()
                            } else {
                                Toast.makeText(this@AkunActivity, model.message, Toast.LENGTH_SHORT).show()
                            }

                            pDialog.dismiss()

                        } catch (e: Exception) {
                            pDialog.dismiss()
                            e.printStackTrace()
                        }

                    } else {
                        pDialog.dismiss()
                        Toast.makeText(this@AkunActivity, response.message(), Toast.LENGTH_SHORT).show()
                    }
                }

                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    pDialog.dismiss()
                    Toast.makeText(this@AkunActivity, t.message, Toast.LENGTH_SHORT).show()
                }
            })

        } else {
            pDialog.dismiss()
            Toast.makeText(this@AkunActivity, "Periksa Jaringan anda.", Toast.LENGTH_SHORT).show()
        }
    }

    //Delete CRUD - Delete
    private fun delete(){
        val pDialog =  SweetAlertDialog(this@AkunActivity, SweetAlertDialog.PROGRESS_TYPE)
        pDialog.progressHelper.barColor = ContextCompat.getColor(this@AkunActivity, R.color.colorPrimary)
        pDialog.titleText = "Loading"
        pDialog.contentText = "Sedang delete data.."
        pDialog.setCancelable(false)
        pDialog.show()

        if (RetrofitInstance().checkKoneksi(this)) {
            val id = SharedPrefManager(this).userID()
            val service = RetrofitInstance().getRetrofitInstance()
            val call = service.deleteAkun(id, SharedPrefManager(this).user())
            call.enqueue(object : Callback<ResponseBody> {
                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    if (response.isSuccessful) {
                        try {
                            rbs = Objects.requireNonNull<ResponseBody>(response.body()).string()
                            model = Gson().fromJson(rbs, object : TypeToken<ModelStatusMessage>() {}.type)

                            if (model.status == 200) {
                                Toast.makeText(this@AkunActivity, model.message, Toast.LENGTH_SHORT).show()
                                logout()
                            } else {
                                Toast.makeText(this@AkunActivity, model.message, Toast.LENGTH_SHORT).show()
                            }

                            pDialog.dismiss()

                        } catch (e: Exception) {
                            pDialog.dismiss()
                            e.printStackTrace()
                        }

                    } else {
                        pDialog.dismiss()
                        Toast.makeText(this@AkunActivity, response.message(), Toast.LENGTH_SHORT).show()
                    }
                }

                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    pDialog.dismiss()
                    Toast.makeText(this@AkunActivity, t.message, Toast.LENGTH_SHORT).show()
                }
            })

        } else {
            pDialog.dismiss()
            Toast.makeText(this@AkunActivity, "Periksa Jaringan anda.", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == HardCode.PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK && data != null && data.data != null) {
            val filePath = data.data!!
            val file = FileUtils.getFile(this,filePath)!!
            multipartBody = multiPart(filePath,file)
            try {
                filePath.let {
                    if(Build.VERSION.SDK_INT < 28) {
                        val bitmap = MediaStore.Images.Media.getBitmap(
                            contentResolver,
                            filePath
                        )
                        // 512 adalah resolusi tertinggi setelah image di resize, bisa di ganti.
                        setToImageView(Gambar.getResizedBitmap(bitmap, 2024)!!)
                    } else {
                        val source = ImageDecoder.createSource(contentResolver, filePath)
                        val bitmap = ImageDecoder.decodeBitmap(source)
                        setToImageView(Gambar.getResizedBitmap(bitmap, 2024)!!)

                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun multiPart(uri: Uri, file: File) : MultipartBody.Part{
        val requestFile = RequestBody.create(
            MediaType.parse("image/*"),
            Gambar.createImageData(this, uri)!!)
        return MultipartBody.Part.createFormData("bukti_pembayaran",file.name.toLowerCase(Locale.ROOT), requestFile)
    }
}