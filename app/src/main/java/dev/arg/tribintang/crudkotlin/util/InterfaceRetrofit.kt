package dev.arg.tribintang.crudkotlin.util

import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

interface InterfaceRetrofit {

    //Base URL terdapat pada HardCode.BASE_URL
    @Multipart
    @POST("register.php")
    fun registerAkun(
        @Part("user_id") user_id: RequestBody,
        @Part("password") password: RequestBody,
        @Part("nama") nama: RequestBody,
        @Part("alamat") alamat: RequestBody,
        @Part("pekerjaan") pekerjaan: RequestBody,
        @Part file: MultipartBody.Part
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("login.php")
    fun loginAkun (
        @Field("user_id") user_id: String,
        @Field("password") password: String
    ) : Call<ResponseBody>

    @Multipart
    @POST("edit.php")
    fun editAkun (
        @Part("id") id: RequestBody,
        @Part("password") password: RequestBody,
        @Part("nama") nama: RequestBody,
        @Part("alamat") alamat: RequestBody,
        @Part("pekerjaan") pekerjaan: RequestBody,
        @Part("user_id") userid: RequestBody,
        @Part file: MultipartBody.Part
    ) : Call<ResponseBody>

    @GET("delete.php")
    fun deleteAkun (
        @Query("id", encoded = true) id: String,
        @Query("gambar", encoded = true) gambar: String
    ) : Call<ResponseBody>
}