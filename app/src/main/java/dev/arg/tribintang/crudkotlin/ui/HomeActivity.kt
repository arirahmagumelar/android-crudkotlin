package dev.arg.tribintang.crudkotlin.ui


import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import dev.arg.tribintang.crudkotlin.R
import dev.arg.tribintang.crudkotlin.ui.akun.AkunActivity
import dev.arg.tribintang.crudkotlin.util.HardCode
import dev.arg.tribintang.crudkotlin.util.SharedPrefManager


class HomeActivity : AppCompatActivity() {
    lateinit var btAkun: Button
    lateinit var imFoto: ImageView

    private var doubleBackToExitPressedOnce = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        btAkun = findViewById(R.id.btAkun)
        imFoto = findViewById(R.id.imFoto)

        val linkFoto = HardCode.BASE_URL + "Gambar/"+SharedPrefManager(this).getFoto()

        Glide.with(this)
            .load(linkFoto)
            .apply(RequestOptions().circleCrop())
            .diskCacheStrategy(DiskCacheStrategy.NONE)
            .skipMemoryCache(true)
            .into(imFoto)

        btAkun.setOnClickListener {
            val intent = Intent(this, AkunActivity::class.java)
            startActivity(intent)
        }


    }

    override fun onBackPressed() {

        @Suppress("UNREACHABLE_CODE")
        if (doubleBackToExitPressedOnce) {
            val intent = Intent(Intent.ACTION_MAIN)
            intent.addCategory(Intent.CATEGORY_HOME)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            return
        }

        this.doubleBackToExitPressedOnce = true
        Toast.makeText(this, "Tap lagi untuk keluar aplikasi..", Toast.LENGTH_SHORT).show()

        Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)
    }

}